import React from "react";
import "../../assets/css/StylePage.css"
import "bootstrap/dist/css/bootstrap.min.css";
import checkList from "../../assets/img/icon_check.png"
import imageService from "../../assets/img/service.png"

const contentStyle = {
    lineHeight : "36px",
    fontSize : "24px"
}
export default function OurService(){
    return(
        <div className=" container-main " id="our-service">
        <div className="container-fluid ">
            <div className="home-section">
                <div className="left">
                    <img src={imageService} alt="images-service " className="images-service " />
                </div>
                <div className=" right">
                    <h2 className="this-header mb-3 ukuran " style= {contentStyle} >Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                    <div className="card-text paragraph " style={{listStyle: "none"}}>
                        <p className="mt-4 ">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                        <li className="mt-3 "> <img src={checkList} alt=" " className="mr-3 " />Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                        <li className="mt-3 ">
                            <img src= {checkList}  alt=" " className="mr-3 " />Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                        <li className="mt-3 "><img src={checkList}  alt=" " className="mr-3 " />Sewa Mobil Jangka Panjang Bulanan</li>
                        <li className="mt-3 "><img src= {checkList}  alt=" " className="mr-3 " />Gratis Antar - Jemput Mobil di Bandara</li>
                        <li className="mt-3 "><img src= {checkList}  alt=" " className="mr-3 " />Layanan Airport Transfer / Drop In Out</li>
                    </div>
                </div>
                <div className="clear"></div>
            </div>
        </div>
    </div>

    )
}
