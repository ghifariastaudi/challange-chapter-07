import React from "react";
import "../../assets/css/StylePage.css"
import "bootstrap/dist/css/bootstrap.min.css";
export default function ContentBanner(){
    return (
        <div className="container-banner mt-5">
        <div className=" container-fluid ">
            <div className="card container-banner-cta " style={{  backgroundColor: "#0D28A6"}}>
                <div className="card-body ">
                    <h2 className=" card-title this-header mt-5 text-white ">Sewa Mobil di (Lokasimu) Sekarang</h2>
                    <div className="card-text ">
                        <p className="paragraph text-white mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                        <div className="this-header mt-5 paragraph ">
                            <button className=" button-green banner-button btn btn-success card-text mb-5 " >Mulai Sewa Mobil
                            </button>
                        </div>
                </div>
            </div>
        </div>
    </div>

    )
}