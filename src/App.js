import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from "./pages/Main";
import Cars from "./pages/Cars"

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/Cars" element={<Cars />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
