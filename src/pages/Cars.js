import React from "react";
import Header from "../components/header/Header";
import ContentMainNoButton from "../components/PageContent/PageContentNoButton";
import Footer from "../components/footer/Footer";

const Cars = ()=>{
    return(
        <div>
            <Header />
            <ContentMainNoButton />
            <Footer />
        </div>
    )
}

export default Cars;