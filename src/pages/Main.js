import React from "react";
import Header from "../components/header/Header";
 import Carousel  from "../components/carousel/Carousel";
 import OurService from "../components/ourservice/OurService";
 import ContentFaq from "../components/faq/Faq";
 import ContentBanner from "../components/banner/Banner";
 import Footer from "../components/footer/Footer";
 import WhyUs from "../components/whyus/WhyUs";
import ContentMain from "../components/PageContent/PageOne";

const index = () => {
    return (
        <div>
            <Header />
            <ContentMain />
            <OurService />
            <WhyUs />
            <Carousel />
            <ContentBanner />
            <ContentFaq />
            <Footer />
        </div>
    )
}

export default index
